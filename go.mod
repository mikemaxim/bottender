module github.com/mmaxim/bottender

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/keybase/go-keybase-chat-bot v0.0.0-20200115000101-884889a5d622
	github.com/stretchr/testify v1.4.0 // indirect
)
